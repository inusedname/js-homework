function reverseString(str) {
    return str.split("").reverse().join("");
}
console.log(reverseString("helloworld"));

/**
 * Hàm split(""): Trả về một mảng string tách được
 * với seperator là param đầu tiên -> cứ gặp là ngắt thành 2 chuỗi
 * - Ở đây do seperator là "": Cứ đi qua một kí tự là ngắt
 * 
 * Hàm reverse(): Trả về mảng đảo ngược của một mảng.
 * 
 * Hàm join(""): Nhận vào một mảng, trả về một string
 * với seperator là param đầu tiên -> giữa 2 item liên tục thì thêm vào string này
 * Mặc định là dấu phẩ
 */