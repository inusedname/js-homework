function longestWord(str) {
    let arr = str.split(/\s+/);
    let longestLen = 0;
    let longestWord;
    for (let s of arr) {
        if (s.length > longestLen) {
            longestLen = s.length;
            longestWord = s;
        }
    }
    return longestWord;
}

/**
 * split(" "): Tách xâu thành mảng các string với tham số seperator truyền vào
 * Seperator là regexp là chuỗi các dấu cách liền nhau
 * 
 * Giải thuật: For trên mảng trên và lưu lại độ dài từ dài nhất cùng từ dài nhất
 * */ 