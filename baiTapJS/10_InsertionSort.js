function insertionSort(inputArr) {
    let n = inputArr.length;
    for (let i = 1; i < n; i++) {
        let current = inputArr[i];

        let j = i - 1;
        while ((j > -1) && (current < inputArr[j])) {
            inputArr[j + 1] = inputArr[j];
            j--;
        }
        inputArr[j + 1] = current;
    }
    return inputArr;
}

/** 
 * Ref: https://stackabuse.com/insertion-sort-in-javascript/
 * 
 * Ý tưởng: Sắp xếp chèn
 * Trong lần lặp hiện tại: Duyệt các phần tử đứng trước nó
 * Liên tục dịch các phần tử lớn hơn nó lên 1 nấc (phần tử đó sẽ bị ghi đè nên phải backup lại)
 * Tới khi gặp phần tử lớn hơn nó thì dừng lại và trả lại phần tử đang xét vào vị trí đó
 */