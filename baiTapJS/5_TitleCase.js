function capitalize(str) {
    str = str.toLowerCase();
    let arr = str.split(" ");
    let res = "";
    for (let s of arr) {
        if (s === "")
            res += " ";
        else
            res += s[0].toUpperCase() + s.slice(1) + " ";
    }
    return res.slice(0, -1);
}

let str = "   heLLo   wOrLD      ";
console.log(str + '$');
console.log(capitalize(str) + '$');
/**
 * split() để tách string thành mảng
 * tạo một string res
 * for lần lượt trên arr sau khi split, cộng res với:
 * ký tự đầu tiên in hoa, sub string từ 1 đến hết in thường,
 * cộng thêm một dấu cách
 * - Trường hợp các dấu cách liền nhau -> hàm split sẽ tạo ra một số string "".
 * Để tái tạo lại thì kiểm tra s === "" nối res với " ".
 * 
 * Trả về: .slice() tới index -1 (length - 1) để loại bỏ dấu cách thừa cuối cấu
 */