function palindrome(str) {
    return str === str.split('').reverse().join('');
}

/**
 * return str === ... : vế phải là chuỗi reverse của vế trái.
 * Nếu hai chuỗi trùng khớp nhau thì đây là xâu palindrome
 */