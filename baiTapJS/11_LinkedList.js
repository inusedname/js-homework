/** Danh sách liên kết đơn:
 * Ref:
 *      https://www.freecodecamp.org/news/implementing-a-linked-list-in-javascript/
 *      https://learnersbucket.com/examples/algorithms/sorting-a-linked-list/
 * 
 * 
 * Danh sách liên kết cấu tạo bởi nhiều node liên kết với nhau
 * Mỗi node lưu: 1) thuộc tính của node đó, 2) tham chiếu tới node kế tiếp node đó
 * 
 * Một số function thêm: size, clear, getLast, getFirst, push_back, push_front, pop_back, pop_front
 * 
 * Sắp xếp:
 * Lặp bắt đầu từ HEAD của dslk
 * Mỗi lần lặp thì thực hiện chèn node đang xét vào danh sách sorted (mặc định là không có gì).
 * 
 * Cách chèn node như sau:
 * - Duyệt từ node HEAD của danh sách sorted, liên tục di chuyển tới node tiếp theo
 * tới khi giá trị của node muốn thêm bắt đầu lớn hơn current.next
 * - Khi này thì gán node muốn thêm.next = nửa danh sách liên kết sau
 * Tail của nửa danh sách đầu.next = node muốn thêm.
 */

function ListNode(data = null, next = null) {
    this.data = data;
    this.next = next;
}

function LinkedList(node = null) {
    this.head = node;
}

LinkedList.prototype.append = function (node) {
    let current = this.head;
    while (current.next != null) {
        current = current.next;
    }
    current.next = node;
}

LinkedList.prototype.toString = function () {
    let current = this.head;
    if (current == null)
        return "";
    let res = "" + current.data;

    while (current.next != null) {
        res += " -> " + current.next.data;
        current = current.next;
    }
    return res;
}

let insertionSort = (head) => {
    let result = null;
    let current = head;
    let next;

    while (current !== null) {
        next = current.next;
        result = sortedInsert(result, current);
        current = next;
    }
    return new LinkedList(result);
}

let sortedInsert = (sorted, newNode) => {
    //Temporary node to swap the elements 
    let temp = new ListNode();
    let current = temp;
    temp.next = sorted;

    //Sort the list based on the specified order
    while (current.next !== null && current.next.data < newNode.data) {
        current = current.next;
    }

    //Swap the elements
    newNode.next = current.next;
    current.next = newNode;

    return temp.next;
}

let linkedList = new LinkedList(new ListNode(9));
linkedList.append(new ListNode(5));
linkedList.append(new ListNode(8));
linkedList.append(new ListNode(1));
linkedList.append(new ListNode(3));
linkedList.append(new ListNode(4));
linkedList.append(new ListNode(6));

console.log(linkedList + "");
let sortedLinkedList = insertionSort(linkedList.head);
console.log(sortedLinkedList + "");