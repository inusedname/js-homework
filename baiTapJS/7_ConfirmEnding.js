function endWith(str, endStr) {
    let start_index = str.length - endStr.length;
    return str.slice(start_index) === endStr;
}

/**
 * start_index: dự đoán vị trí bắt đầu của endStr trên str
 * kiểm tra sub string bắt đầu từ start_idx của str có trùng khớp với endStr không
 */