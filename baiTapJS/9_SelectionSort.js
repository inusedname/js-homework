function selectionSort(inputArr) {
    let n = inputArr.length;

    for (let i = 0; i < n; i++) {
        let min = i;
        for (let j = i + 1; j < n; j++) {
            if (inputArr[j] < inputArr[min]) {
                min = j;
            }
        }
        if (min != i) {
            // Swapping the elements
            let tmp = inputArr[i];
            inputArr[i] = inputArr[min];
            inputArr[min] = tmp;
        }
    }
    return inputArr;
}

/**
 * Ref: https://stackabuse.com/selection-sort-in-javascript/
 * 
 * Mảng sẽ được chia làm 2 nửa: nửa đầu sắp xếp đúng, nửa sau chưa sắp xếp
 * Ban đầu nửa đầu không có ptu nào, nửa sau là tất cả
 * 
 * Tìm kiếm phần tử bé nhất trong nửa sau lần lặp này và
 * di chuyển nó tới nửa đầu. 
 * Sau mỗi lần lặp nửa đầu.length++, nửa sau.length-- tới khi nửa sau.length về 0
 */