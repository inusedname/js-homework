function max(arr) {
    arr = arr.filter(it => !isNaN(it));
    return Math.max(...arr);
}

console.log(max([1, 2, 3, 4, "2454adwdw"]))

/** 
 * Dùng filter(lambda) để lọc ra những cái là số, còn lại bỏ
 * Math.max(n1, n2, n3..., nN) mà đầu vào là array
 * => sử dụng spread operator để tách mảng -> các item
 *  */