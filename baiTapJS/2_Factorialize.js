function factorialize(num) {
    if (num < 0)
        return NaN;
    if (num < 19) {
        let res = 1;
        for (let i = 1; i <= num; i++)
            res *= i;
        return res;
    }
    else {
        let res = 1n;
        for (let i = 1; i <= num; i++) {
            res *= BigInt(i);
        }
        return res;
    }
}

/** 
 * Num âm: trả về NaN
 * Num < 19: Number nằm trong giới hạn an toàn (2^53 - 1): có thể trả về Number
 * Num >= 19: Vượt quá giới hạn: Sử dụng BigInt
 * 
 * Giải thuật: Giai thừa = tích từ 1 tới n
 */
