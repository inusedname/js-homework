function bubble(arr) {
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                let temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    return arr;
}

/**
 * Reference: https://www.geeksforgeeks.org/bubble-sort-algorithms-by-using-javascript/
 * 
 * Sắp xếp nổi bọt:
 * Độ phức tạp O(n^2)
 * - Bắt đầu từ cặp pt thứ 0 và 1, có thể đổi chỗ 2 phần tử để đảm bảo phần tử lớn hơn sẽ đứng trước
 * - Lặp lại như thế đối với cặp (1,2), (2,3)...
 * - Kết thúc vòng lặp i, phần tử thứ n - i - 1 sẽ là max của arr từ [0, n - i - 1]
 * - Tiếp tục lặp n lần, hoặc kiểm tra xem vòng lặp đó có xảy ra sự thay đổi vị trí không.
 * Nếu không thì mảng đã được sắp xếp xong.
 * - Do ở mỗi vòng lặp, phần tử lớn nhất ở vòng lặp đó (không tính những đứa lớn hơn ở vòng lặp trước)
 * từ từ di chuyển lên đầu, nên người ta gọi là sắp xếp nổi bọt
 */